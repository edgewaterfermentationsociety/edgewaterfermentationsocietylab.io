---
layout: post
title: "2018 Loveall Competition"
date: 2018-05-05
categories: update
---

### First Round

24 entries were accepted into 4 style groups for judging. Entrants were assigned a group and style by the judging team by March 8th. Each person brewed their beers and submitted them the first week in May for the BJCP judging.

On May 5th, 10 brewers brought their beers to the Edgetucky Derby Party, a local get together in the neighborhood with a history of homebrew. The people have spoken and the winner of the “People’s Choice” and automatic entry into the Final “Best of Show” Round was…

**Lindsay Kincade** with **Blondie Blonde Ale**
<br/>[Download Blondie Recipe](/images/post/2018-loveall-blondie-recipe.pdf)
<img src="/images/post/2018-loveall-blondie.png" alt="Blondie Blond Ale"/>

### Final "Best of Show" Round

Round 1 winners will be assigned 1-2 adjunct ingredients by the judging team in early May. Brewers can choose beer style of their liking to showcase the assigned ingredients.

The final round will be judged as a “Best of Show”!

“Best of Show” beer drop-off will be the first week of July, and winners will be announced soon after.

### Grand Prize

The “Best of Show” beer will then be brewed with the Joyride Brewing team on their 15-barrel system to be served on August 25th at the Edgewater Community Festival, with proceeds benefiting Edgewater Collective.

In addition to your beer being served, the winner gets one whole year of bragging rights.

Also, Tom’s Brew Shop and CO Brew have kicked in prizes, for the winners. Be sure to support these local shops who have been great supporters of the club…

### Sponsors

Our great sponsors below have helped promote the contest and offered up some great prizes for our winners! Whether or not you are in the competition, please take your business to these great local shops for all your beer and brewing needs!!!

<a href="https://joyridebrewing.com/">
<img src="/images/sponsor/joyride.jpg" alt="Joyride Brewing"/>
</a>

<a href="https://tomsbrewshop.com/">
<img src="/images/sponsor/toms_brew_shop.png" alt="Tom's Brew Shop"/>
</a>

<img src="/images/sponsor/co_brew.png" alt="Co-Brew Homebrew Shop"/>
