---
layout: post
title: "2017 Loveall Competition Results"
date: 2017-05-13
categories: update
---

## Entries

The following entries were received into the 2017 Loveall Open!!!

_Coconut porter, Vanilla cream ale, Saison with Brett, New England style IPA, American IPA, Jalapeno Maibock, Blood orange Saison, Honey Lager, Black IPA with coconut, American amber, Cherry stout, Mango wheat, American pale ale, saison with peach, Belgian dubbel, Imperial red IPA, Belgian blonde, Belgian golden strong, Irish red_

## Best of Show

_Sponsored by **Joyride Brewing Compony**_

1st Place (Gold)
: Antonio Muniz - "Camacho Jr." Pineapple Saison

2nd Place (Silver)
: Jamie Moore - "Tangled Up in Red" IPA

3rd Place (Bronze)
: Jamie Moulton - "Along the Way" Coconut Black IPA

Honorable Mention
: Chris Mendez - "Garage a Trois" Brett Saison

## Peoples Choice

_Sponsored by **Tom's Brew Shop**_

1st Place (Gold)
: Jon Lefor - "Pretty Hot for 40" Jalapeno Helles-inspired Bock

2nd Place (Silver)
: Jason Baldwin - "Blood Orange You Glad It's Spring" Saison

3rd Place (Bronze) 3-way tie
: Travis Jones - "Saftigol" Juicy Beer
: Jared King - "Conjugal Coconut Porter"
: Charlie Hood - "Oatmeal Stout w/ Raspberries"

Honorable Mention
: None (because of 3 way tie)

## Judges

<img src="/images/post/2017-loveall-judges.jpg" alt="Photo of 2017 Loveall Open Judges"/>

## Sponsors

Our great sponsors below have helped promote the contest and offered up some great prizes for our winners! Whether or not you are in the competition, please take your business to these great local shops for all your beer and brewing needs!!!

<a href="https://joyridebrewing.com/">
<img src="/images/sponsor/joyride.jpg" alt="Joyride Brewing"/>
</a>

<a href="https://tomsbrewshop.com/">
<img src="/images/sponsor/toms_brew_shop.png" alt="Tom's Brew Shop"/>
</a>

<a href="https://www.rootshootmalting.com/">
<img src="/images/sponsor/root_shoot.png" alt="Root Shoot Malting Company"/>
</a>
