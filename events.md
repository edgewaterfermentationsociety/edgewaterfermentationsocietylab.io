---
layout: page
title: Events
permalink: /events/
---

### Monthly Meetings

We meet on the 2nd Thursday of every month at rotating locations in and around
Edgewater from 7:00 till whenever. Our format is loose… bring a bottle or two
to share of your latest, an oldie but goodie, experiment, whatever. We
occasionally have guest speakers, surprise beers or other beer-centric
on-goings.

### Calendar

Have an event to share?
<a href="mailto:info@edgewaterfermentationsociety.org">
    Email info@edgewaterfermentationsociety.org
</a> to have it added to the group calendar</a>

<iframe src="https://calendar.google.com/calendar/embed?height=300&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FDenver&amp;src=ZWRnZXdhdGVyZmVybWVudGF0aW9uc29jaWV0eUBnbWFpbC5jb20&amp;color=%23039BE5&amp;showTitle=0&amp;showTabs=0&amp;showCalendars=0&amp;showPrint=0&amp;showDate=1&amp;showNav=1&amp;mode=AGENDA" style="border-width:0" width="100%" height="300" frameborder="0" scrolling="no"></iframe>
