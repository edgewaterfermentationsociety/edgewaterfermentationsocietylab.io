---
layout: default
title: Home
---

The Edgewater Fermentation Society (_formerly Yeasty Bottoms_) was
born organically after a number of friends in Edgewater, Colorado, realized
they were all into homebrewing at various levels. One of us suggested we
bring beers to a party, then another said
_“wait, how about we brew batches specifically for a party?”_
After many years of such homebrew-centered gatherings, eventually the loose-knit
club was formalized and registered with the AHA in 2016.

**But it doesn’t stop there.** Among us are makers of other
fermented beverages, including: beer, cider, kombucha, mead, wine, jun tea,
and various fermented foods. Have a passion or interested in getting one?
Join us!

We give you, The Edgewater Fermentation Society…

<figure>
  <img
    src="/images/EFS_Logo_RGB_large.png"
    alt="Edgewater Fermentation Society Logo"
  />
  <figcaption>
    <small>(Logo created by Todd Niebur | Mountain Creative Design)</small>
  </figcaption>
</figure>

### Recent Posts

{% for post in site.posts limit: 3 %}

<h4>
  <a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
  <small>({{ post.date | date: "%b %-d, %Y" }})</small>
</h4>

{% endfor %}
